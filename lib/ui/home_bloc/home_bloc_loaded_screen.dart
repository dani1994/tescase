import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:majootestcase/models/movie.dart';
import 'package:majootestcase/models/movie_response.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Data> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return movieItemWidget(data[index], context);
      },
    ),
    );
  }

  Widget movieItemWidget(Data data, context){
    return GestureDetector(
      onTap: (){
        if(data.series.isNotEmpty){
            Navigator.push(context, MaterialPageRoute(builder: (context) => HomeDetailBlocLoadedScreen(data: data)));
        }
      },
      child: Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)
          )
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(data.i.imageUrl),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text(data.l, textDirection: TextDirection.ltr),
          )
        ],
      ),
    ));
  }
}


class HomeDetailBlocLoadedScreen extends StatelessWidget {
   final Data data;

  const HomeDetailBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding : false,
      body: Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)
          )
      ),
        child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(data.i.imageUrl),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text("Title Movie: "+data.l, textDirection: TextDirection.ltr),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
            child: Text("Tahun Movie: "+data.year.toString(), textDirection: TextDirection.ltr),
          ),
          Padding(
            padding: EdgeInsets.all(25),
            child: Container(
          child: SizedBox(
            height: 180.0,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                for (int datai = 0; datai < data.series.length ; datai++)
                Container(
                  width: 160.0,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)
                        )
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(0),
                          child: Image.network(data.series[datai].i.imageUrl,height: 120),
                        ),
                        Padding(
                          padding: EdgeInsets.all(2),
                          child: Text(data.series[datai].l, style: TextStyle(fontSize: 10), textDirection: TextDirection.ltr),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
            // ListView.builder(
            //   shrinkWrap: true,
            //   scrollDirection: Axis.horizontal,
            //   itemCount: data.series.length,
            //   itemBuilder: (context, idx) {
            //     print("TSDAKSFA ");
            //     return itemseries(data);
            //   },
            // ),
          )
        ],
      ),
      ),
    );
  }
  Widget itemseries(Data data){
    return Container(
      height: 200,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0)
              )
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(25),
                child: Image.network(data.i.imageUrl),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(data.l, textDirection: TextDirection.ltr),
              )
            ],
          ),
        ));
  }
}
