import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/login/login_page.dart';



class RegisterPage extends StatefulWidget {
  @override
  _RegisterPage createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage> {
  final _emailregisterController = TextController();
  final _passwordregisterController = TextController();
  final _usernameregisterController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan register terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                _login(),
              ],
            ),
          ),
        ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailregisterController,
            isEmail: true,
            hint: 'example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null){
                  if(!pattern.hasMatch(val) && val != ""){
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text(
                        'Masukkan e-mail yang valid',
                        )
                      ));
                    }
                // return pattern.hasMatch(val) ? null : 'email is invalid';
              }
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            hint: 'password',
            label: 'Password',
            controller: _passwordregisterController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            controller: _usernameregisterController,
            hint: 'username',
            label: 'Username'
          ),
        ],
      ),
    );
  }

  Widget _login() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegister() async {
      final _email = _emailregisterController.value;
      final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
      final _password = _passwordregisterController.value;
      final _username = _usernameregisterController.value;
      if (formKey.currentState?.validate() == true &&
          _email != null &&
          _password != null && _username != null && pattern.hasMatch(_email)
        ) {

        AuthBlocCubit authBlocCubit = AuthBlocCubit();
        User user = User(
            email: _email,
            password: _password,
            userName: _username
        );
        authBlocCubit.registeruser(user);
        setState(() {
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyApp()));
        });
      } else {
        if(_email == null && _password == null && _username == null || _email == "" && _password == "" && _username == ""){
          _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
            'Form tidak boleh kosong, mohon cek kembali data yang anda inputkan',
            )
          ));
        }

        
      }
    
  }

}
