import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/database_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());
  DatabaseHelper con = new DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void fetchhistorylogin() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn == null){
      emit(AuthBlocLoginState());
    } else {
      if(isLoggedIn){
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
      
    }
  }

  Future<User> login(String email, String password) async {
    var dbClient = await con.db;
    var res = await dbClient.rawQuery("SELECT * FROM user_login WHERE email = '$email' and password = '$password'");
    if (res.length > 0) {
      return new User.fromJson(res.first);
    }

    return null;
  }

  void loginuser(User user) async {
      var cek = await login(user.email ,user.password);
      if(cek != null){
        SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
        await sharedPreferences.setBool("is_logged_in",true);
        emit(AuthBlocLoggedInState());
      } else {
        Fluttertoast.showToast(
            msg: "Login gagal",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Color(0xFFD32F2F),
            textColor: Color(0xFFFFFFFF),
            fontSize: 16.0
        );
      }
  }

//insertion
  Future<int> saveUser(User user) async {
    var dbClient = await con.db;
    int res = await dbClient.insert("user_login", user.toMap());
    return res;
  }

  void registeruser(User user) async{
      var cek = await saveUser(user);
      if(cek != null){
          Fluttertoast.showToast(
                msg: "Register Berhasil",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 1,
                backgroundColor: Color(0xFF689F38),
                textColor: Color(0xFFFFFFFF),
                fontSize: 16.0
            );
      } else {
        Fluttertoast.showToast(
                msg: "Register Gagal",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 1,
                backgroundColor: Color(0xFFD32F2F),
                textColor: Color(0xFFFFFFFF),
                fontSize: 16.0
            );
      }
  }
}
