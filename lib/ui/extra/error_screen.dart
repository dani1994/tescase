import 'dart:io';

import 'package:flutter/material.dart';

import '../../main.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final Function() retry;
  final Color textColor;
  final double fontSize;
  final double gap;
  final Widget retryButton;

  const ErrorScreen(
      {Key key,
      this.gap = 10,
      this.retryButton,
      this.message="",
      this.fontSize = 14,
      this.retry,
      this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              message,
              style: TextStyle(
                  fontSize: 30, color: textColor ?? Colors.black),
            ),
            Icon(
              Icons.wifi_off,
              color: Colors.green,
              size: 50.0,
            ),
            TextButton(
              style: TextButton.styleFrom(
                primary: Colors.blue,
              ),
              onPressed: () async { 
                try {
                  final result = await InternetAddress.lookup('google.com');
                  if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyApp()));
                    print('connected');
                  }
                } on SocketException catch (_) {
                  print('not connected');
                }
              },
              child: Text('REFRESH'),
            ),
            retry != null
                ? Column(
                    children: [
                      SizedBox(
                        height: 100,
                      ),
                      retryButton ??
                          IconButton(
                            onPressed: () {
                              if(retry!=null)
                                retry();
                            },
                            icon: Icon(Icons.refresh_sharp),
                          ),
                    ],
                  )
                : SizedBox()
          ],
        ),
      ),
    );
  }
}
